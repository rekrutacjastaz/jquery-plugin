/*jshint jquery: true */

(function ($) {
    'use strict';
    $.fn.lolvalidator = function (action, options) {
        var that = this;
        if (action === 'form') {
            this.addClass('formChangedEvent');
            var validateForm = function () {
                var disable = false;
                that.find('input').each(function (idx, elem) {
                    if ($(elem).hasClass('error')) {
                        disable = true;
                    }
                    that.find(':submit').prop('disabled', disable);
                    return that;
                });
            };
            return this.on('formChangedEvent', validateForm);
        }

        if (action === 'regex') {
            var regexSettings = $.extend({
                regex: '*'
            }, options);

            var validateRegex = function () {
                if (that.val().match(regexSettings.pattern)) {
                    that.removeClass('error');
                    that.addClass('ok');
                    $('.formChangedEvent').trigger('formChangedEvent');
                    return that;
                } else {
                    that.removeClass('ok');
                    that.addClass('error');
                    $('.formChangedEvent').trigger('formChangedEvent');
                    return that;
                }
            };

            validateRegex();
            return this.on('blur', validateRegex);
        }

        if (action === 'email') {
            var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
            var validateEmail = function () {
                if (that.val().match(pattern)) {
                    that.removeClass('error');
                    that.addClass('ok');
                    $('.formChangedEvent').trigger('formChangedEvent');
                    return that;
                } else {
                    that.removeClass('ok');
                    that.addClass('error');
                    $('.formChangedEvent').trigger('formChangedEvent');
                    return that;
                }
            };

            validateEmail();
            return this.on('blur', validateEmail);
        }

        if (action === 'password') {
            var passwordSettings = $.extend({
                lowercase: 1,
                uppercase: 1,
                digits: 1,
                special: 1
            }, options);

            var validatePassword = function () {
                var s = that.val();
                var numLower = s.length - s.replace(/[a-z]/g, '').length;
                var numUpper = s.length - s.replace(/[A-Z]/g, '').length;
                var numDigits = s.length - s.replace(/[0-9]/g, '').length;
                var numSpecial = s.replace(/[a-z]/g, '').
                replace(/[A-Z]/g, '').replace(/[0-9]/g, '').length;
                if (numLower >= passwordSettings.lowercase &&
                    numUpper >= passwordSettings.uppercase &&
                    numDigits >= passwordSettings.digits &&
                    numSpecial >= passwordSettings.special) {
                    that.removeClass('error');
                    that.addClass('ok');
                    $('.formChangedEvent').trigger('formChangedEvent');
                    return that;
                } else {
                    that.removeClass('ok');
                    that.addClass('error');
                    $('.formChangedEvent').trigger('formChangedEvent');
                    return that;
                }
            };

            validatePassword();
            return this.on('blur', validatePassword);
        }

        if (action === 'passwordEx') {
            var passwordExSettings = $.extend({
                minimalEtropy: 60,
                meter: undefined,
                bar: undefined
            }, options);

            var validatePasswordEx = function () {
                var s = that.val();
                var numLower = s.length - s.replace(/[a-z]/g, '').length;
                var numUpper = s.length - s.replace(/[A-Z]/g, '').length;
                var numDigits = s.length - s.replace(/[0-9]/g, '').length;
                var numSpecial = s.replace(/[a-z]/g, '').
                replace(/[A-Z]/g, '').replace(/[0-9]/g, '').length;
                var x = 0;
                if (numLower > 0) {
                    x += 26;
                }
                if (numUpper > 0) {
                    x += 26;
                }
                if (numDigits > 0) {
                    x += 10;
                }
                if (numSpecial > 0) {
                    x += 33;
                }

                var entropy = 0;
                if (x > 0) {
                    entropy = s.length * Math.log2(x);
                }

                if (passwordExSettings.meter !== undefined) {
                    passwordExSettings.meter.text(entropy.toFixed(2));
                }
                
                var bar = passwordExSettings.bar;
                if (bar !== undefined) {
                    bar.prop('max', 5);
                    var p = entropy / passwordExSettings.minimalEtropy;
                    if (p === 0) {
                        bar.prop('value', 0);
                    } else if (p < 0.25) {
                        bar.prop('value', 1);
                    } else if (p < 0.50) {
                        bar.prop('value', 2);
                    } else if (p < 0.75) {
                        bar.prop('value', 3);
                    } else if (p < 1.0) {
                        bar.prop('value', 4);
                    } else if (p >= 1.0) {
                        bar.prop('value', 5);
                    }
                }

                if (entropy > passwordExSettings.minimalEtropy) {
                    that.removeClass('error');
                    that.addClass('ok');
                    $('.formChangedEvent').trigger('formChangedEvent');
                    return that;
                } else {
                    that.removeClass('ok');
                    that.addClass('error');
                    $('.formChangedEvent').trigger('formChangedEvent');
                    return that;
                }
            };

            validatePasswordEx();
            return this.on('keyup', validatePasswordEx);
        }

        if (action === 'postcode') {
            var validatePostCode = function () {
                if (that.val().match(/^\d{2}-\d{3}$/)) {
                    if (options !== undefined && options.locality !== undefined) {
                        $.getJSON('./postcodes.json', function (data) {
                            for (var i = 0; i < data.codes.length; i++) {
                                var obj = data.codes[i];
                                if (obj.postcode === that.val()) {
                                    options.locality.val(obj.locality);
                                    that.removeClass('error');
                                    that.addClass('ok');
                                    $('.formChangedEvent').trigger('formChangedEvent');
                                    return that;
                                }
                            }
                            options.locality.val('Brak miejscowości o podanym kodzie pocztowym');
                            that.removeClass('ok');
                            that.addClass('error');
                            $('.formChangedEvent').trigger('formChangedEvent');
                            return that;
                        });
                    } else {
                        that.removeClass('error');
                        that.addClass('ok');
                        $('.formChangedEvent').trigger('formChangedEvent');
                        return that;
                    }
                } else {
                    if (options !== undefined && options.locality !== undefined) {
                        options.locality.val('');
                    }
                    that.removeClass('ok');
                    that.addClass('error');
                    $('.formChangedEvent').trigger('formChangedEvent');
                    return that;
                }
            };

            validatePostCode();
            return this.on('keyup', validatePostCode);
        }
    };
})(jQuery);