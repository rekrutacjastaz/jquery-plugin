/*jshint jquery: true, globalstrict: true, browser: true, devel: true */

'use strict';

$('#form').lolvalidator('form');

$('#regex').lolvalidator('regex', {
    pattern: /^[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłóńśźż]*$/,
});

$('#email').lolvalidator('email');

$('#password1').lolvalidator('password', {
    lowercase: 2,
    uppercase: 2,
    digits: 2,
    special: 3
});

$('#password2').lolvalidator('passwordEx', {
    minimalEtropy: 75,
    meter: $('#meter'),
    bar: $('#password-strength-bar')
});

$('#postcode').lolvalidator('postcode', {locality: $('#locality')});