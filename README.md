# jQuery validation plugin

## Wymagania
* walidacja w *jslint*/*jshint* ✔
* zabezpieczenie aliasu *$* ✔
* chaining ✔
* wykorzystanie *each* ✔
* przyjmowanie parametrów ✔

## Funkcjonalność
* walidacja na podstawie wyrażenia regularnego ✔
* walidacja pola e-mail ✔
* walidacja na podstawie złożoności hasła (nieoparta o wyrażenia regularne) ✔
* walidacja złożoności hasła - entropia większa od zadanej ✔
* obsługa pola kodu pocztowego: ✔
    * pole w formacie *dd-ddd* - wtyczka wypełnia pole z nazwą miejscowości ✔
    * niepoprawny format - czerwone obramowanie wokół pola ✔
    * baza kodów pocztowych pobierana dopiero w momencie wpisania kodu pocztowego ✔
* blokowanie przycisku "wyślij" ✔
    
## Dodatkowo
* poprawny html ✔
* bootstrap ✔
* podgląd entropii na żywo ✔
* pasek wizualizujący siłę hasła ✔
* wybór akcji na podstawie parametru (użycie tylko jednego slotu w $.fn) ✔
* własne zdarzenie (zmiana w formularzu) ✔
* licznik entropii i pole miejscowości jako opcjonalne parametry ✔